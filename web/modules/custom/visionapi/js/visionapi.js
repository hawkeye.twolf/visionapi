(function ($) {
  "use strict";
  Drupal.behaviors.visionapi = {
    attach: function (context, drupalSettings) {

      // Make the gallery advance on click.
      $('.visionapi-colorbox img', context).once().click(function () {
        $.colorbox.next();
      });

      // Make Drupal JS process the "use-ajax" approve/reject links we add to
      // the Colorbox modal title.
      $(document).once('visionapi').bind('cbox_complete', function() {
        Drupal.attachBehaviors();
      });

      // Add colorbox functionality to each thumbnail.
      $('[data-visionapi-nid]', context).once('visionapi').each(function () {
        var $link = $(this);
        var row = 'visionapi-nid-' + $link.data('visionapi-nid');
        var selector = '.visionapi-row.' + row;
        var settings = $.extend({}, drupalSettings.colorbox, {
          href: false,
          inline: true
        }, {
          className: 'visionapi-dashboard ' + row,
          rel: 'visionapi',
          maxWidth: '100%',
          href: selector + ' .visionapi-colorbox',
          title: function () {
            return $(selector + ' .visionapi-buttons-wrapper').html();
          }
        });
        $link.colorbox(settings);
      });
    }
  };
})(jQuery);
