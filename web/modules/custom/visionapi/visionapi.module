<?php

/**
 * @file
 * Contains visionapi.module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\file\FileInterface;
use Drupal\visionapi\EntityOperations;
use Google\Cloud\Vision\V1\Likelihood;
use Google\Cloud\Vision\VisionClient;

/**
 * Implements hook_help().
 */
function visionapi_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the visionapi module.
    case 'help.page.visionapi':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Google Vision API') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_presave().
 *
 * Checks content for suspect images and sets the moderation state accordingly.
 */
function visionapi_entity_presave(EntityInterface $entity) {
  return \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(EntityOperations::class)
    ->entityPresave($entity);
}

/**
 * Implements hook_entity_insert().
 */
function visionapi_entity_insert(EntityInterface $entity) {
  return \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(EntityOperations::class)
    ->entityView($entity);
}

/**
 * Implements hook_entity_update().
 */
function visionapi_entity_update(EntityInterface $entity) {
  return \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(EntityOperations::class)
    ->entityView($entity);
}

/**
 * Implements hook_entity_view_mode_alter().
 */
function visionapi_entity_view_mode_alter(&$view_mode, EntityInterface $entity, $context) {
  if ('full' === $view_mode) {
    \Drupal::service('class_resolver')
      ->getInstanceFromDefinition(EntityOperations::class)
      ->entityView($entity);
  }
}

/**
 * Implements hook__page_attachments().
 */
function visionapi_page_attachments(array &$attachments) {
  if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
    $attachments['#attached']['library'][] = 'visionapi/messages';
  }
  else {
    $attachments['#attached']['library'][] = 'core/jquery.ui.button';
    $attachments['#attached']['library'][] = 'visionapi/dashboard';
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for node_recipe_edit_form().
 */
function visionapi_form_node_recipe_edit_form_alter(&$form, &$form_state) {
  // Always default to draft state on new edits.
  $form['moderation_state']['widget']['0']['state']['#default_value'] = 'draft';
}
