<?php

namespace Drupal\visionapi;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\file\FileInterface;
use Drupal\user\UserInterface;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Google\Cloud\Vision\V1\Likelihood;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for reacting to entity events.
 *
 * @internal
 */
class EntityOperations implements ContainerInjectionInterface {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Moderation Information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityOperations object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_info
   *   Moderation information service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, ModerationInformationInterface $moderation_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->moderationInfo = $moderation_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('content_moderation.moderation_information')
    );
  }

  /**
   * Determines whether an image has suspect content.
   *
   * @param \Drupal\file\FileInterface $image
   *   The image file object.
   *
   * @return bool
   *   TRUE if the image is suspect, FALSE otherwise.
   */
  protected static function imageIsSuspect(FileInterface $image) {
    $image_data = fopen($image->uri->value, 'r');
    $visionapi = new ImageAnnotatorClient(['credentials' => 'private://service_account.json']);
    $safe_search = $visionapi
      ->safeSearchDetection($image_data)
      ->getSafeSearchAnnotation();
    foreach (['Adult', 'Medical', 'Racy', 'Violence'] as $property) {
      if ($safe_search->{"get$property"}() >= Likelihood::LIKELY) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Determines whether an entity contains a suspect image.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check for a suspect image.
   *
   * @TODO: Image source is hard-coded; make it configurable for contrib.
   */
  protected static function contentHasSuspectImage(ContentEntityInterface $entity) {
    if (empty($entity->field_image->entity)) {
      return TRUE;
    }
    return static::imageIsSuspect($entity->field_image->entity);
  }

  /**
   * Flags content with suspcicious imagery for review.
   *
   * Note that this code must run before
   * \Drupal\content_moderation\EntityOperations::entityPresave(). Use module
   * weights in core.extension configuration to effect this order.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved.
   *
   * @see hook_entity_presave()
   */
  public function entityPresave(EntityInterface $entity) {

    // Do not moderate the moderators.
    // @TODO: If not attempting to publish, still show the warning as usual.
    if (static::userCanModerate()) {
      return;
    }

    // Make sure this entity is under moderation.
    if (!$this->moderationInfo->isModeratedEntity($entity)) {
      return;
    }

    // Only flag for review if the user is trying to publish it.
    if ('published' !== $entity->moderation_state->value ?? NULL) {
      return;
    }

    // Is the image suspect?
    if (static::contentHasSuspectImage($entity)) {
      $entity->moderation_state->value = 'review';
    }
  }

  /**
   * Displays a context-aware message about status of the current revision.
   *
   * Some code adapted from:
   * \Drupal\content_moderation\EntityOperations::entityView().
   */
  public function entityView(EntityInterface $entity) {
    // Make sure this entity is under moderation.
    if (!$this->moderationInfo->isModeratedEntity($entity)) {
      return;
    }
    // Notify on preview, even though its not a real moderation state.
    if (isset($entity->in_preview) && $entity->in_preview) {
      $this->notify($entity, 'preview');
      return;
    }
    // Only act on latest revisions, and only when they are not published.
    if (!$entity->isLatestRevision() && !$entity->isLatestTranslationAffectedRevision()) {
      return;
    }
    $moderation_state = $entity->moderation_state->value ?? NULL;
    if ($entity->isDefaultRevision() || $entity->wasDefaultRevision() && $moderation_state) {
      $workflow = $this->moderationInfo->getWorkflowForEntity($entity);
      if ($workflow->getTypePlugin()->getState($moderation_state)->isPublishedState()) {
        return;
      }
    }
    $this->notify($entity);
  }

  /**
   * Determines if a user has permission to moderate.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to check.
   */
  protected static function userCanModerate(UserInterface $user = NULL) {
    if (is_null($user)) {
      $user = \Drupal::currentUser();
    }
    return $user->hasPermission('use visionapi transition reject');
  }

  /**
   * Logs notifications to the screen.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity with a possibly-suspect image.
   * @param string $moderation_state
   *   The entity's current moderation state.
   */
  protected function notify(ContentEntityInterface $entity, $moderation_state = NULL) {
    $moderation_state = $moderation_state ?? $entity->moderation_state->value ?? 'draft';
    $terms = '#';
    $route_parameters = [
      'entity_type' => $entity->getEntityTypeId(),
      'revision_id' => $entity->getRevisionId(),
    ];
    switch ($moderation_state) {
      // Preview is not a real moderation state.
      case 'preview':
        if (static::contentHasSuspectImage($entity)) {
          $this->messenger->addWarning(
            t("The image below may violate this site's <a href=':terms'>terms of use</a> and would require approval before going live.", [
              ':terms' => $terms,
            ])
          );
        }
        break;

      case 'draft':
        // On entity presave, we get double-notified.
        if (empty($route_parameters['revision_id'])) {
          break;
        }
        if (static::contentHasSuspectImage($entity)) {
          $this->messenger->addWarning(
            t("The image below may violate this site's <a href=':terms'>terms of use</a> and requires approval before going live.<a class='button' href=':review'>Submit for review & publication</a>", [
              ':terms' => $terms,
              ':review' => Url::fromRoute('visionapi.review', $route_parameters)->toString(),
            ])
          );
        }
        else {
          $message = t("Changes are ready to go live. <a class='button' href=':publish'>Publish</a>", [
            ':publish' => Url::fromRoute('visionapi.publish', $route_parameters)->toString(),
          ]);
          if (static::userCanModerate()) {
            $link = Link::createFromRoute(
              t('Flag for review'), 'visionapi.review', $route_parameters);
            // When passed as a plain string, addMessage() escapes markup.
            $message = Markup::create("$message " . $link->toString());
          }
          $this->messenger->addMessage($message);
        }
        break;

      case 'review':
        if (static::userCanModerate()) {
          $this->messenger->addWarning(
            t("Please determine whether the suspect image below adheres to this site's <a href=':terms'>terms of use</a>.<a class='button' href=':approve'>Approve</a><a href=':reject'>Reject</a>", [
              ':terms' => $terms,
              ':approve' => Url::fromRoute('visionapi.approve', $route_parameters)->toString(),
              ':reject' => Url::fromRoute('visionapi.reject', $route_parameters)->toString(),
            ])
          );
        }
        else {
          $this->messenger->addWarning(
            t("These changes will go live once moderators confirm that the suspect image below adheres to this site's <a href=':terms'>terms of use</a>.", [
              ':terms' => $terms,
            ])
          );
        }
        break;

      case 'rejected':
        $this->messenger->addError(
          t("Moderators rejected these changes because the image violates this site's <a href=':terms'>terms of use</a>.", [
            ':terms' => $terms,
          ])
        );
        break;

      default:
    }
  }

}
