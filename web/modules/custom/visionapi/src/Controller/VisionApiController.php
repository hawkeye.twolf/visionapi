<?php

namespace Drupal\visionapi\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VisionApiController.
 */
class VisionApiController extends ControllerBase {

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The File System service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new VisionApiController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File System service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('file_system')
    );
  }

  /**
   * Attempts to publish a content revision.
   *
   * @param string $entity_type
   *   The entity type of the content to be moderated.
   * @param string|int $revision_id
   *   The ID of the content entity to be moderated.
   */
  public function sendForReview($entity_type, $revision_id) {
    $revision = $this->getRevision($entity_type, $revision_id);
    $entity = $this->entityTypeStorage($entity_type)->createRevision($revision, $revision->isDefaultRevision());
    return $this->moderate($entity_type, $revision_id, 'review',
      $this->t("Submit suspect image for review."),
      $revision->isDefaultRevision()
        ? "entity.$entity_type.canonical"
        : "entity.$entity_type.latest_version"
    );
  }

  /**
   * Approves content flagged as suspect via Google Vision API.
   *
   * @param string $entity_type
   *   The entity type of the content to be moderated.
   * @param string|int $revision_id
   *   The ID of the content entity to be moderated.
   */
  public function approve($entity_type, $revision_id) {
    $entity = $this->getRevision($entity_type, $revision_id);
    $moderation_state = $entity->moderation_state->value ?? 'draft';
    if (!static::isAjax()) {
      $message = 'review' === $moderation_state
        ? $this->t('You approved the suspect image; all changes in its associated content revision are now live.')
        : $this->t('Changes are live.');
      $this->messenger()->addStatus($message);
    }
    $reason = 'review' === $moderation_state
      ? $this->t('Suspect image approved.')
      : $this->t('Publishing without modification.');
    return $this->moderate($entity_type, $revision_id, 'published', $reason,
      "entity.$entity_type.canonical"
    );
  }

  /**
   * Rejects content for Terms of Use violation.
   *
   * @param string $entity_type
   *   The entity type of the content to be moderated.
   * @param string|int $revision_id
   *   The ID of the content entity to be moderated.
   */
  public function reject($entity_type, $revision_id) {
    if (!static::isAjax()) {
      $this->messenger()->addError(
        $this->t('You rejected the suspect image and with it all changes in its associated content revision.')
      );
    }
    return $this->moderate($entity_type, $revision_id, 'rejected',
      $this->t("Image violates this site's Terms of Use."),
      "entity.$entity_type.canonical"
    );
  }

  /**
   * Loads an entity revision.
   *
   * @param string $entity_type
   *   The entity type of the content to be moderated.
   * @param string|int $revision_id
   *   The ID of the content entity to be moderated.
   */
  protected function getRevision($entity_type, $revision_id) {
    static $revisions = [];
    $revision = &$revisions[$entity_type][$revision_id];
    if (isset($revision)) {
      return $revision;
    }
    return $revision = $this->entityTypeStorage($entity_type)
      ->loadRevision($revision_id);
  }

  /**
   * Gets the entity type storage.
   */
  protected function entityTypeStorage($entity_type) {
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    static $storages = [];
    $storage = &$storages[$entity_type];
    if (!isset($storage)) {
      return $storage = $this->entityTypeManager
        ->getStorage($entity_type);
    }
    return $storage;
  }

  /**
   * Approves or rejects content with a suspect image.
   *
   * @param string $entity_type
   *   The entity type of the content to be moderated.
   * @param string|int $revision_id
   *   The ID of the content entity to be moderated.
   * @param string $new_state
   *   Moderation state to be set.
   * @param string $reason
   *   The message to log with this entity revision.
   * @param string $destination
   *   The destination route.
   *
   * @see \Drupal\content_moderation\Form::submitForm()
   */
  protected function moderate($entity_type, $revision_id, $new_state, $reason, $destination) {
    $revision = $this->getRevision($entity_type, $revision_id);
    $entity = $this->entityTypeStorage($entity_type)
      ->createRevision($revision, $revision->isDefaultRevision());
    $id = $entity->id();

    // Update the entity's moderation state.
    $entity->moderation_state = $new_state;
    if ($entity instanceof RevisionLogInterface) {
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionLogMessage($reason);
      $entity->setRevisionUserId($this->currentUser()->id());
    }
    $entity->save();

    // Respond to AJAX request from Dashboard.
    if (static::isAjax()) {
      $ajax = new AjaxResponse();
      $result = ucwords($new_state);
      $ajax->addCommand(new HtmlCommand(
        ".visionapi-nid-$id .visionapi-buttons", "<em class='visionapi-$new_state'>$result</em>"));
      $ajax->addCommand(new InvokeCommand(
        ".visionapi-nid-$id.visionapi-row", 'css', ['background-color', static::getStateColor($new_state)]));
      return $ajax;
    }

    // Redirect to published version.
    return $this->redirect($destination, [$entity_type => $id]);
  }

  /**
   * Determines whether the current request is Ajax or not.
   */
  protected static function isAjax() {
    static $result;
    if (!isset($result)) {
      $result = \Drupal::request()->isXmlHttpRequest();
    }
    return $result;
  }

  /**
   * Returns the correct color for the given state.
   */
  protected static function getStateColor($state) {
    switch ($state) {
      case 'published':
        return '#f3faef';

      case 'rejected':
        return '#fcf4f2';

      default:
        return '#fdf8ed';
    }
  }

}
